//
// Created by C19Zachary.Martinez on 4/30/2017.
//


#include<stdlib.h>
#include "Graph.h"
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>

#define FALSE   0
#define TRUE   1
Graph *graphCreate(int numberVertices, int bytesPerNode) {
    Graph *graph = (Graph *) malloc(sizeof(Graph));

    graph->numberVertices = numberVertices;
    graph->vertices = malloc(numberVertices * bytesPerNode);
    graph->multipleTwo = (int *) malloc(numberVertices * sizeof(int));
    graph->edges = (int **) malloc(numberVertices * sizeof(int *));

    for (int j = 0; j < numberVertices; j++) {
        graph->edges[j] = (int *) malloc(numberVertices * sizeof(int));
    }

    for (int row = 0; row < graph->numberVertices; row++) {
        for (int col = 0; col < graph->numberVertices; col++) {
            graph->edges[row][col] = 0;
        }
    }

    return graph;
}

/// deletes the graph
/// \param graph
void graphDelete(Graph *graph) {


    for (int row = 0; row < graph->numberVertices; row++) {
        for (int col = 0; col < graph->numberVertices; col++) {
            free(graph->edges[row][col]);
        }
        free(graph->edges[row]);
    }

    free(graph->vertices);
    free(graph);
}

/// Sets up the 2d array for weights between nodes
/// \param graph
/// \param fromVertex
/// \param toVertex
/// \param state 0,1, or 2 depending on state
void graphSetEdge(Graph *graph, int fromVertex, int toVertex, int state) {
    fromVertex = fromVertex - 0;
    toVertex = toVertex - 0;
    if (fromVertex >= 0 && fromVertex <= graph->numberVertices &&
        toVertex >= 0 && toVertex <= graph->numberVertices) {
        graph->edges[fromVertex][toVertex] = state;
    }
}

/// returns the weight of an edge between 2 nodes
/// \param graph
/// \param fromVertex
/// \param toVertex
/// \return weight of an edge
int graphGetEdge(Graph *graph, int fromVertex, int toVertex) {
    return graph->edges[fromVertex][toVertex];
}

/// Runs dijkstras on the graph.
/// \param graph
/// \param startnode
/// \return a custom struct used to handle the 2d array slightly differently
void * findShortestPath(Graph *graph,int startnode) {
    Dijkstra* temp = (Dijkstra*)malloc(sizeof(Dijkstra));
    temp->distance = (int *) malloc(5 * sizeof(int));
    temp->prevNodeArray = (int *) malloc(5 * sizeof(int));

    int cost[graph->numberVertices][graph->numberVertices];

    int visited[graph->numberVertices];
    int count;
    int mindistance;
    int nextnode;
    int i, j;

    //prevNodeArray[] stores the predecessor of each node
    //count gives the number of nodes seen so far
    //init cost
    for (i = 0; i < graph->numberVertices; i++)
        for (j = 0; j < graph->numberVertices; j++)
            if (graph->edges[i][j] == 0)
                cost[i][j] = 999;
            else
                cost[i][j] = graph->edges[i][j];

    //init prevNodeArray[],distance[] and visited[]
    for (i = 0; i < graph->numberVertices; i++) {
        temp->distance[i] = cost[startnode][i];
        temp->prevNodeArray[i] = startnode;
        visited[i] = 0;
    }


    temp->distance[startnode] = -1;
    visited[startnode] = 1;
    count = 1;

    while (count < graph->numberVertices - 1) {
        mindistance = 999;

        //nextnode gives the node at minimum distance
        for (i = 0; i < graph->numberVertices; i++)

            if (temp->distance[i] < mindistance && !visited[i]) {
                mindistance = temp->distance[i];
                nextnode = i;
            }

        //check if a better path exists through nextnode
            visited[nextnode] = 1;
            for (i = 0; i < graph->numberVertices; i++)
                if (!visited[i])
                    if (mindistance + cost[nextnode][i] < temp->distance[i]) {

                        temp->distance[i] = mindistance + cost[nextnode][i];
                        temp->prevNodeArray[i] = nextnode;
                    }
        count++;
    }

        ///print for debugging
//    for (i = 0; i < graph->numberVertices; i++)
//        if (i != startnode) {
//            printf("\nDistance of node %d=%d", i, temp->distance[i]);
//            printf("\nPath= %d", i);
//
//            j = i;
//            do {
//                j = temp->prevNodeArray[j];
//                printf("<-%d", j);
//            } while (j != startnode);
//        }

    return temp;
}