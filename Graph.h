/** Graph.h
 * ===========================================================
 * Name: Dr. Wayne Brown, Spring 2017
 * Modified by: <YOUR NAME HERE>
 * Section:
 * Project: PEX4
 * Purpose: The definition of a graph.
 * ===========================================================
 */

#ifndef GRAPH_H
#define GRAPH_H

#include<stdlib.h>

typedef struct graph{
	int    numberVertices;
	void * vertices;  // Array of nodes
    int * multipleTwo;
	int ** edges;     // Adjacency matrix (2D Array)
} Graph;

typedef struct dijkstras{
    int *   distance;
    int *   prevNodeArray;  // Array of prevnodes
} Dijkstra;

Graph * graphCreate(int numberVertices, int bytesPerNode);
void    graphDelete(Graph *graph);
void    graphSetEdge(Graph *graph, int fromVertex, int toVertex, int state);
int     graphGetEdge(Graph *graph, int fromVertex, int toVertex);
void *   findShortestPath(Graph *graph ,int fromVertex);

#endif // GRAPH_H